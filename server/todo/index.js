const express = require('express');
const app = express();
const todoRouter = express.Router();

let todoId = 0;
const todos = [
  {
    title:"Go to store to get milk and eggs.",
    completed:false,
    id:0
  }
];

todoRouter.get('/', (req, res) => {
  res.send(todos);
})

todoRouter.get('/:id', (req, res) => {
  const todo = todos.find(t => t.id === +req.params.id);
  res.send(todo)
})

todoRouter.post('/', (req, res) => {
  const newTodo = {
    id: todoId++,
    title: req.body.title,
    completed: req.body.completed
  }
  todos.push(newTodo);
  res.send(newTodo);
})

todoRouter.put('/:id', (req, res) => {
  const index = todos.findIndex(t => t.id === +req.params.id);
  todos[index].title = req.body.title;
  todos[index].completed = req.body.completed;
  res.sendStatus(200);
})

todoRouter.delete('/:id', (req, res) => {
  const index = todos.findIndex(t => t.id === +req.params.id);
  res.send(todos.splice(index, 1)[0])
})

module.exports = {
    path: "/todos/",
    handler: todoRouter
};
