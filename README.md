# My Todo list Project

> This is a training project for using Vuejs and the Jest testing framework.

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

Other links:
* https://vuejs.org/v2/guide/instance.html (Vuejs Lifecycle Diagram)
* https://stackoverflow.com/questions/26618243/how-do-i-read-an-istanbul-coverage-report (Code Coverage)
* https://vuex.vuejs.org/ (Vuex - State management)


## Project Steps

# 1 - pages/index.vue - Display the correct number of found items

* Add the following test to the existing test/index.spec.js file:

``` bash
  test('shows the number of items found', () => {
    const wrapper = shallowMount(Index,{store, localVue})
    expect(wrapper.find('#todoCount').text()).toBe("Found 2 items");
  }) 
```
* Run the test using "npm run test_index", it should fail.

* Fix the index.vue file so the test will pass.

# 2 - Create TodoList Component

Create a component that adds a table to list all the Todo items.

a) Create the following files:

    component/TodoList.vue

    test/components/TodoList.spec.js

b) Create a basic component by adding the following to the TodoList.vue file

``` bash
  <template>
    <table></table>
  </template>

  <script>
    export default {
      name: "todoList"
    }
  </script>

  <style scoped>

  </style>
```

c) Update test/components/TodoList.spec.js file by coping the contents of the index.spec.js file, then update the import statement and references to use "TodoList" rather than index.

d) Run the test using the following command, it should pass

    npm run test_list

e) Test that the table is not displayed if there are no items in the todo list by adding the following test. Then run it, it should fail!

``` bash
  test('hides the table if there are no todo items', () => {
    store = new Vuex.Store({
        modules: {
          todo: {
            namespaced: true,
            state: () => ({
              todoList: []
            }),
            actions: {
              getTodoAction: jest.fn()
            }
          }
        }
      });
    const wrapper = mount(TodoList,{store, localVue});
    expect(wrapper.find("table").exists()).toBe(false);
  })
```

f) Make the test pass, add computed and v-if statement to the component, now it should pass.

``` bash
  computed: {
    todoList: {
      get() {
        return this.$store.state.todo.todoList;
      }
    }
  }

  v-if="todoList.length > 0"
```
g) Test the snapshot, then run, it should pass, we will revisit this again.

``` bash
  test("renders correctly", () => {
    const wrapper = mount(TodoList, {store, localVue});
    expect(wrapper).toMatchSnapshot();
  })
```

h) Test the content of the table, run the test, it should fail.

``` bash
  test('shows the correct number of items, in the correct order', () => {
    const wrapper = mount(TodoList,{store, localVue})

    let rows = wrapper.findAll("tbody tr");
    //console.log("HTML: " + wrapper.html());
    expect(rows.length).toBe(2);
    expect(rows.at(0).text()).toContain("Go to the hardware store.");
    expect(rows.at(1).text()).toContain("Cut the grass.");
  })
```

i) Now make the test pass by adding the correct table content:

``` bash
    <table border="1" v-if="todoList.length > 0">
      <thead>
        <tr>
          <th>Completed</th>
          <th>Title</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(todo,idx) in todoList" :key="idx" class="todoItemRow">
          <td>
              <input type="checkbox" id="checkbox" v-model="todo.completed" disabled>
          </td>
          <td>{{todo.title}}</td>
          <td>delete</td>
        </tr>
      </tbody>
    </table>
```

j) Notice that the stored snapshot failed, run the test again using the following command to update the snapshot:

    npm run test_list -- -u

# 3 - Add the component to the index page.


a) Open pages/index.vue, and add the following to the Script and template senctions

``` bash
<script>
  import TodoList from '@/components/TodoList.vue';
  ...
  components: {
    TodoList
  }

<template>
  ...
  <todo-list/>

```
    
b) View the http://localhost:3000 page in your browser to see the table.

# 4 - Add TodoItem Component

a) Create the following files:

    component/TodoItem.vue

    test/components/TodoItem.spec.js

b) Create a basic component by adding the following to the TodoItem.vue file

``` bash
  <template>
    <tr>
    </tr>
  </template>

  <script>
    export default {
      name: "todoItem"
    }
  </script>

  <style scoped>

  </style>
```

c) Create the basic two tests by adding the following to the test/components/TodoItem.spec.js file. Run the test and see that both tests will pass, "npm run test_item"

``` bash
  import { mount, createLocalVue } from '@vue/test-utils'
  import TodoItem from '@/components/TodoItem.vue'
  import Vuex from 'vuex'
  import sinon from 'sinon'

  const localVue = createLocalVue();
  localVue.use(Vuex);

  describe('TodoItem', () => {

    let testItem = {
        title:"Take Vuejs training.",
        completed:true
    }

    test('is a Vue instance', () => {
        const wrapper = mount(TodoItem,{
            propsData: {
                index:0,
                item: testItem
            }
        });
        expect(wrapper.isVueInstance()).toBe(true);
    })

    test("renders correctly", () => {
      const wrapper = mount(TodoItem, {
        propsData: {
            index:1,
            item: testItem
        }
      });

      expect(wrapper).toMatchSnapshot()
    })
  })

```

d) Add the test to verify that the row cells are created correctly. Run this test and it should fail,

    Note: To run just one test, or skip a test you can use the syntax:

        test.only(...) - run just this test
        xtest(...) - skip this test


``` bash
  test("displays the props which are passed in correctly", () => {
      const wrapper = mount(TodoItem, {
          propsData: {
              index:1,
              item:testItem
          }
      });

      let cells = wrapper.findAll("td");
      expect(cells.length).toBe(3);
      expect(cells.at(0).find("input").element.checked).toEqual(true);
      expect(cells.at(1).text()).toBe("Take Vuejs training.");

      //console.log("HTML: " + wrapper.html());
    
  })
```

e) Now add a test that uses a spy to verify that the deleteItem method is called when the delete button is selected. 

``` bash
  test("called the deleteItem method when the delete button is selected", () => {
    let store = new Vuex.Store({
      modules: {
        todo: {
          namespaced: true,
          actions: {
            deleteTodoAction: jest.fn()
          }
        }
      }
    });

    const deleteItemSpy = sinon.spy(TodoItem.methods, 'deleteItem')
    const wrapper = mount(TodoItem, { store, localVue ,
        propsData: {
            index:1,
            item:testItem
        }
    });

    expect(wrapper.find("#deleteButton_1").exists()).toBe(true);
    wrapper.find("#deleteButton_1").trigger("click");
    expect(deleteItemSpy.calledOnce).toBe(true);

  })
```

f) Now update the component with the following so the two test will pass.

``` bash
  <template>
    <tr :id="'item_' + index">
      <td>
        <input type="checkbox" id="checkbox" v-model="item.completed" disabled>
      </td>
      <td>{{item.title}}</td>
      <td>        
        <button :id="'deleteButton_' + index" v-on:click.prevent="deleteItem(item)" type="button"
          class="deleteButton">delete
        </button>
      </td>
    </tr>

  <script>
    props: ["item", "index"],
    methods: {
      deleteItem(item) {
        this.$store.dispatch("todo/deleteTodoAction", item);
      }
    }

```

    Note: the "props" defined here are passed into the component as attributes on the component tag:

        <todoItem item="{}", index="1"/>

    Rerun the test and the snapshot will fail because the template changed. Rerun the test with the "-- -u" flag to update the snapshop and see the test pass.

# 5 - Add the todoItem component into the TodoList

* TAdd the TodoItem component into the TodoList component, this component should replace the existing tr tag with the following todo-item tag. Also, don't forget to import the TodoItem component. 


``` bash
<todo-item v-for="(item,idx) in todoList" :key="idx" :item="item" :index="idx"/>
```

Run the test for the TodoList (npm run test_list) and it should still pass, but the snapshot may need to be updated.

# 6 - Assignment: Create a Component called AddTodo with tests, add it to the index.vue page.

a) Create the vue and spec file for the new component.

    components/AddTodo.vue

    test/components/AddTodo.spec.js

b) Add the two tests to the spec file to test the the component exists and that it renders correctly.

c) Add the following template, script and css to the vue file, then run the test and see that they pass.

``` bash
<template>
  <div class="addTodo">
    <h2>Create Todo</h2>
    <div class="row">
      <div class="col-auto pr-0">
        <label for="title" class="form-label">Title</label>
        <input type="text" class="form-control form-control-sm" 
            name="title" id="title" v-model="title">
      </div>
    </div>

    <button id="createTodoButton" type="button">Create
    </button>
  </div>
</template>

<script>
export default {
  name:'addTodo',
  data() {
    return {
      title:""
    }
  },
  methods: {
  }
}
</script>

<style scoped>
    .addTodo {
        margin-top: 20px;
    }
</style>

```

d) Create a test using a spy that checks that the createTodo method is called in the component with the "Create" button is selected. Run the test and it will fail because the method does not exist yet.

``` bash
  test("calls createTodo method when the create button is selected", () => {
    let store = new Vuex.Store({
      modules: {
        todo: {
        namespaced: true,
          actions: {
            addTodoAction: jest.fn()
          }
        }
      }
    })

    const createTodoSpy = sinon.spy(AddTodo.methods, 'createTodo');
    const wrapper = mount(AddTodo, {store,localVue});
    expect(wrapper.find("#createTodoButton").exists()).toBe(true);
    wrapper.find("#createTodoButton").trigger("click");
    expect(createTodoSpy.calledOnce).toBe(true);

  })
```
e) Update the button in the template so the createTodo method is called, and add the method to the script. Run the test and it should pass.

``` bash
<template>
   add "v-on:click.prevent="createTodo"" to button

<script>
    createTodo: function() {
      this.$store.dispatch("todo/addTodoAction", this.getTodo());
    },
    getTodo: function() {
      let newTodo =  {
        title: this.title,
        completed:false
      }

      return newTodo;
    }
```

f) Add a test to verify that when I enter something into the title filed and call the getTodo method a valid todo object is returned. Run this test and it will fail, see if you can figure out why.

``` bash
  test("todo is created correctly based on title entered into the entry field", () => {
    const wrapper = mount(AddTodo); 
    wrapper.find("#title").setValue("Do something nice.")
    
    expect(wrapper.vm.title).toBe("Do something nice.");

    let newTodo = wrapper.vm.getTodo();
    expect(newTodo.title).toBe("Do something nice.");
    expect(newTodo.completed).toBe(false);
    expect(wrapper.find("#title").element.value).toBe("");
  })
```

g) When you have the testing working add this component to index.vue 
