import { mount, shallowMount, createLocalVue  } from '@vue/test-utils'
import Index from '@/pages/index.vue'
import Vuex from 'vuex'

const localVue = createLocalVue();
localVue.use(Vuex);

const todoData = [
  {
    title:"Go to the hardware store.",
    completed:false
  },
  {
    title:"Cut the grass.",
    completed:false
  }
]

describe('Index page', () => {
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        todo: {
          namespaced: true,
          state: () => ({
            todoList: todoData
          }),
          actions: {
            getTodoAction: jest.fn()
          }
        }
      }
    })
  })

  test('is a Vue instance', () => {
    const wrapper = mount(Index,{store, localVue})
    expect(wrapper.isVueInstance()).toBe(true)
  })

})