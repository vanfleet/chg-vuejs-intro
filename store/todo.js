import axios from 'axios'
  
export const state = () => ({
  todoList: []
})

export const mutations = {
  setTodos: function(state,todos) {
    state.todoList=todos;
  },
  addTodo: function (state, todo) {
    state.todoList.push(todo);
  },
  deleteTodo: function (state, id) {
   const index = state.todoList.findIndex(t => t.id === id);
   state.todoList.splice(index,1);
  },
  updateTodo(state, todo) {
    const index = state.todoList.findIndex(t => t.id === todo.id);
    state.todoList[index]=todo;
  }
}

export const getters = {    
}

export const actions = {
  // get list from backend
  async getTodoAction ({ commit }) {
    const todos = await axios.get('/todos').catch(err => {
        console.error("Error getting todo list: " + err);
    });

    // Save to local store
    commit('setTodos', todos.data)
  },

  async deleteTodoAction ({ commit }, item) {
    // Delete from backend
    const todo = await axios.delete('/todos/' + item.id).catch(err => {
        console.error("Error deleting todo item: " + err);
    });

    // Delete from store
    commit('deleteTodo', item.id);
  },

  async addTodoAction ({ commit }, item) {

    // Add to backend
    const newTodo = await axios.post('/todos', item).catch(err => {
        console.error("Error adding todo item: " + err);
    });

    // Add to store
    commit('addTodo', newTodo.data);
  }
}
